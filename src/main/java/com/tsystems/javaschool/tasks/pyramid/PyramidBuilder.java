package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

	/**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
	//PyramidBuilder test implementation for T-Systems by Svyatoslav Kosovskih
		public int[][] buildPyramid(List<Integer> num){
			boolean isBuild = false;
			int pyramid[][];
			int len = num.size(); // List length
			//Exception in case of big data
			if (len>1000) {
				throw new CannotBuildPyramidException("BigDataError");
			}
			//Exception in case of null value
			if (num.contains(null)) {
				throw new CannotBuildPyramidException("NullHandleException");
			}
			//Rows and columns counting
			int rows = 1; int cols = 1; int count = 1;
			for (int n : num) {
				rows++;
				cols=cols+2;
				count += rows;
				if (count == len) {
					isBuild = true;
					break;
				}
			}
			
			//Making sure if pyramid is buildable
			if (isBuild) {
			Collections.sort(num);	
			pyramid = new int[rows][cols];
			int c = 0;
			int cnt = 1;
			for (int i = 0; i < rows; i++, cnt++) {
				//Filling each line with zeros
				for (int j = 0; j <= cols-1; j++) {
					pyramid[i][j] = 0;
				}
				//Numbers input oriented by matrix center
				for(int k = 0; k < cnt*2; k+=2, c++) {
					pyramid[i][cols/2-i+k] = num.get(c);	
				}
							
			}
			//Exception Throwing
			} else {
				throw new CannotBuildPyramidException("CannotBuild");
			}
			return pyramid;
		}
}
