package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
	
	//Calculator test implementation for T-Systems by Svyatoslav Kosovskih
	//Checking if token is operand
public boolean isNum(char token){
        
        if ((token >= '0') && (token <= '9')){
            return true;
        }
        else {
            return false;
        }
    }
    //Checking if token is operator
    public boolean isOperator(char token){
        switch (token) {
            case '+':
            case '-':
            case '*':
            case '/':
                return true;
            default:
                return false;
        }
    }
    //Checking the operations precedence 
    public int opPrecedence (String token) {
        switch (token) {
            case ")":
                return 4;
            case "(":
                return 0;
            case "*":
            case "/":
                return 2;
            
            default:
                return 1;
        }
    }
    //Math operations
	public double calculation(Stack<Double> num, Stack<String> op) {
        double val1 = num.pop();
        double val2 = num.pop();
        String operator = op.pop();
        Double result = null;
        switch (operator){
            case "+":
                result = val2 + val1;
                break;
            case "-":
                result = val2 - val1;
                break;
            case "*":
                result = val2 * val1;
                break;
            case "/":
            	if (val1 != 0) {
            		result = val2 / val1;
            	}
            	else {result = null;}
            	break;
        }
        return result;
       
    }
    //Checking if c is empty
    public boolean isEval(String c) {
    	if (c == null || c.isEmpty()) {
        	return false;
        }
    	else {
    		return true;
    	}
    }
    //Final result rounding
    //If number is integer then outputs it without dot
    public String doubleRound (Double output) {
    	DecimalFormat formattedDouble = new DecimalFormat("#.####", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        return (formattedDouble.format(output)); 
    }
    //Main method
    public String evaluate(String c){
        Stack<Double> num = new Stack<Double>();
        Stack<String> op = new Stack<String>();
        
        if (isEval(c) == false) {
        	return null;
        }
        
        for (int i=0;i<c.length();i++){
            char token = c.charAt(i);
            String symb = Character.toString(token);
            
            if (isNum(token)) {
            	symb = "";
            	
            	while (i < c.length() && ((c.charAt(i) == '.') || (isNum(c.charAt(i))))) {
            		
            		symb += c.charAt(i);
            		
            		i++;
            	}
            	i--;
            	try { 
            		Double nom = Double.parseDouble(symb);
            		num.push(nom);
            	}catch (Exception e) {
            		return null;
            	}
                
            }
            
            else if (isOperator(token)) {
            	
                while ((op.size() != 0) && (opPrecedence(symb) <= opPrecedence(op.peek()))){
                	try {
                    Double res = calculation(num, op);
                    num.push(res);
                	} catch (Exception e) {
                		return null;
                	}
                }
            	
                op.push(symb);
            	
            }
            
            else if (token == '(') {
                    op.push(symb);
            }
            //If closing bracket -> pop all operators and puts them in number stack until the peek 
            //Operator is opening bracket
            else if (token == ')') {
            		
                    while (op.peek().charAt(0) != '(') {
                    	try {
                        num.push(calculation(num, op)); }
                        catch (Exception e) {
                			return null;
                		}
                    } 
                    op.pop();
            		}
            		
                }
        
            /*if (op.size() >= 2 && (opPrecedence(numb) <= opPrecedence(op.peek()))){
                
                
                System.out.println(res);
            }*/
        	//Finally puts all operators and puts them in operand stack
        	//Until stack is empty
        	while (op.size() > 0) {
        		try {
                num.push(calculation(num, op));
        		} catch (Exception e){
                	return null;
                }
        }
        //The last value in operand stack is the calculator output
        Double output = num.peek();
        if (op.isEmpty() && num.size() == 1) {
        	return doubleRound(output);
        }
        else {
        	return null;
        }
        
    }

}
