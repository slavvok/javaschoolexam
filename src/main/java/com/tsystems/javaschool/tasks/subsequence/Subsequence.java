package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
	
	//Subsequence test for T-Systems by Svyatoslav Kosovskih
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y){
    	//Exception Handling
    	if (x == null || y == null) {
    		throw new IllegalArgumentException();
    	}
    	int count = 0;
    	int currIndex = 0;
    	//
		for (Object i : x) {
			for (Object j : y) {
				if(i == j) {
					count ++;
					//Index comparison
					//Next element can't go before last
					if (y.indexOf(j)<currIndex) {
						return false;
					}
					currIndex = y.indexOf(j);
					break;
				}
			}
		}
		
		//Count shows how many times x appears in y
		//If values are equal - function returns true
		if (count == x.size() ) {
			return true;
		}
		else {return false;}
    }
    	
}
   

